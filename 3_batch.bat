@echo off
chcp 65001
setlocal EnableDelayedExpansion

echo Дана програма рахує кількість підкаталозів у заданій директорії
set /p "dirpath=Введіть шлях до директорії: "

if not exist "%dirpath%" (
    echo Дана директорія не знайдена!
    pause >nul
    exit /b
)

set /a count=0
for /f "delims=" %%d in ('dir /a:d /b /s "%dirpath%"') do (
    set /a count+=1
)

echo Кількість підкаталозів(також враховані і приховані каталоги" %count%.
echo Дякую, що скористалися програмою, для виходу натисніть будь яку клавішу!
pause >nul

